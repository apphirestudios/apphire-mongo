"use strict";

if (typeof window !== "undefined" && window !== null) {
	module.exports = require('./lib/frontend');
} else {
	module.exports = eval('require("./lib/backend")');
}
