'use strict'
mongoose = require 'mongoose'
_ = require 'underscore'
async = require 'co'
path = require 'path'

attributeIsObjectType = (attr)->
  #FIXME - traverse through schema and determine attribute type
  if attr is 'projectId' or attr is 'accountId' then true else false

castToObjectId = (query)->
  for k, v of query
    if typeof v == 'object'
      query[k] = castToObjectId v
    else
      if attributeIsObjectType k
        query[k] = mongoose.Types.ObjectId v
  return query

module.exports = mongo = 
  Schema: (fields)->
      schema = new mongoose.Schema(_.extend fields, @attributes)
      _.extend schema, @commands
      schema.set 'toJSON', transform: (doc, ret, options) ->
        ret.id = ret._id
        delete ret._id
        delete ret.__v
        return ret
      return schema   
  attributes: {}
  transformers: []    
  commands:
    statics:
      findOneD: (query, fields, options)-> async =>
        applyTransformers(query, 'findOneD')
        result = yield @findOne query, fields, options
        if not result? then throw new Error(@model.modelName + ' not found')
        return result

      findD: (query, fields, options)-> async =>    
        applyTransformers(query, 'findD')
        yield @find(query, fields, options).exec()

      countD: (query)-> async =>
        applyTransformers(query, 'countD')
        yield @count query

      removeD: (query)-> async =>
        applyTransformers(query, 'removeD')
        yield @remove query

      aggregateD: (query)-> async =>
        applyTransformers(castToObjectId(query), 'findOneD')
        #TODO  - make that recursive
        yield @aggregate(query).exec()

      findOneAndUpdateD: (query, patch, options)-> async =>
        applyTransformers(query, 'findOneAndUpdateD')
        result = @findOneAndUpdate query, patch, options
        if not result? then throw new Error(@model.modelName + ' not found')
        return result
    
    methods:
      saveD: ()-> async =>
        applyTransformers(@, 'saveD')
        yield @save()
  model: mongoose.model.bind mongoose       

do -> async ->
  app.notify 'Connecting to MongoDB'
  try
    app.connection = yield new Promise (resolve, reject)->          
      db = mongoose.connection
      db.on 'error', reject
      db.once 'open', -> resolve db
      mongoose.connect process.env.MONGO_URL
    app.notify 'Connecting to MongoDB. SUCCESS.'
  catch e
    app.error 'Error connecting to MongoDB. Check that MongoDB server is running and dbConnectionString is correct.'
    app.error e.stack
  
mongo.transformers.push (query)->
  if query.id
    query._id = query.id
    delete query.id
  return query

mongo.transformers.push (query)->
  #TODO - make that recursive
  for attr, val of query
    if val?['$regex']?
      query[attr] = new RegExp('' + val['$regex'] + '', 'i')

applyTransformers = (queryOrDocument)->
  for transform in mongo.transformers
    transform(queryOrDocument)  

if app.config.modules.mongo.softDelete
  require('./soft-delete')(mongo)

mongo.Schema.Types = mongoose.Schema.Types    