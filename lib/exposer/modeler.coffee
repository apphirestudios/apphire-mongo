serializer = require './serializer'
'use strict'
module.exports =
  initialize: (app)->
    app.router.post '/api/:modelName/:command', (next)->
      command = @params.command
      query = @request.body.query or {}
      fields = @request.body.fields
      options = @request.body.options
      customArgumens = @request.body.arguments or []
      doc = @request.body.document or {}
      model = @request.model
      #doc = _.omit doc, 'accountId'

      switch command
        when 'Schema' then @body = serializer.serialize model
        when 'find' then @body = yield model.findD query, fields, options
        when 'findOne' then @body = yield model.findOneD query, fields, options
        when 'count' then @body = yield model.countD query
        when 'aggregate' then @body = yield model.aggregateD query
        when 'remove' then @body = yield model.removeD query
        when 'save'
          if doc.id #If we have 'id' in request then it is new document
            document = yield model.findOneD {_id: doc.id}
            document.set(doc)
            @body = yield document.saveD()
          else #otherwise we are updating existing
            newDocument = new model doc
            @body = yield newDocument.saveD()
        else
          customArgumens.push @request
          if model[command]
            @body = yield model[command].apply(model, customArgumens)
          else
            if doc.id
              document = yield model.findOneD {_id: doc.id}
              document.set(doc)
              @body = yield document[command].apply(document, customArgumens)
            else
              newDocument = new @request.model doc
              @body = yield newDocument[command].apply(newDocument, customArgumens)
      yield next
