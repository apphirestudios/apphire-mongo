'use strict'
module.exports =
  initialize: (app)->
    app.router.post '/api/:modelName/:command',  (next)->
      userRole = @request.user?.role or 'GUEST'
      requestedCommand = @params.command
      modelName = @request.modelName = @params.modelName

      isGlobalAccess = @request.model.isGlobal or false
      if @request.model.permissions
        userPermissions = @request.model.permissions[userRole]
        if userPermissions
          if userPermissions.indexOf(requestedCommand) == -1
            throw new Error 'Command ' + requestedCommand + ' on model ' + modelName + ' is not allowed for user ' + userRole
          else
            if userRole == 'ADMIN' then isGlobalAccess = true
            if @request.model.schema.methods?[requestedCommand]?.isGlobal then isGlobalAccess = true

            if not isGlobalAccess and @params.command isnt 'Schema'
              targetField = if @request.modelName is 'Account' then '_id' else 'accountId'
              if @params.command is 'aggregate'
                tf = {}
                tf[targetField] = @request.account.id
                accountFilter =[{ $match: tf }]
                @request.body.query = accountFilter.concat(@request.body.query or [])
              else
                @request.body.query = @request.body.query or {}
                @request.body.query[targetField] = @request.account.id
                @request.body.document = @request.body.document or {}
                @request.body.document[targetField] = @request.account.id

            yield next
        else
          throw new Error 'User role ' + userRole + ' has not granted to access model ' + modelName
      else
        throw new Error 'Permissions for model ' + modelName + ' has not been set'
