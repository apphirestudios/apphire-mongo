'use strict'
_ = require 'lodash'

hasObjectsInAttributes = (obj)->
  res = false
  for k, v of obj
    if (typeof v == 'object') and not (v instanceof Array) and k isnt 'default'
      #FIXME 'default' attribute can be date, which is object, try smth better for finding if obj is leaf of the schema tree
      res = true
  res

parseSchemaNode = (node)->
  if node instanceof Array
    res = []
    res[0] = parseSchemaNode node[0]
  else
    if not hasObjectsInAttributes node
      if node?.serverOnly == true
        res = null
      else
        res = _.cloneDeep(node)
        if res?.type? and res?
          switch node.type.toString()
            when String.toString() then res.type = 'String'
            when Object.toString() then res.type = 'Object'
            when Boolean.toString() then res.type = 'Boolean'
            when Date.toString() then res.type = 'Date'
            when Number.toString() then res.type = 'Number'
            else   res.type = 'UNKNOWN'


    else
      res = {}
      for attribute, description of node
        attr = parseSchemaNode description
        #If recursive func returned smth not null then add result to hash
        if attr and attribute isnt '_id' then res[attribute] = attr

        #id is not required field on client side
        if attribute is 'id' then res.id = {required: false, type: 'Object'}
  res


serializeSchema = (model)->
  schema = model.schema.tree
  serializedSchema = parseSchemaNode schema
  serializedSchema.statics = {}
  serializedSchema.methods = {}
  for fName, fn of model.schema.statics
    serializedSchema.statics[fName] = {}
  for fName, fn of model.schema.methods
    serializedSchema.methods[fName] =
      disableTypeCasting: fn.disableTypeCasting?
  if serializedSchema.__v then delete serializedSchema.__v
  return serializedSchema


module.exports =
  serialize: serializeSchema