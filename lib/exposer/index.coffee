'use strict'
mongoose = require 'mongoose'
mongoose.Promise = global.Promise
modeler = require './modeler'
authentifier = require './authentifier'
authorizer = require './authorizer'
moment = require 'moment'

module.exports =
  expose: (app, mongo, UserModel, AccountModel, secretToken, cookieName)->
    authentifier.initialize app, UserModel, AccountModel, secretToken, cookieName

    app.router.post '/api/:modelName/:command',  (next)->
      modelName = @params.modelName
      requestedCommand = @params.command
      if process.env.LOG_LEVEL >= 2
        console.log modelName + '::' + requestedCommand
      models = mongo.connection.modelNames()
      if (m for m in models when m is modelName).length is 0
        throw new Error 'Model name ' +   modelName + ' not found'
      @request.model = mongoose.model(modelName)
      yield next

    authorizer.initialize app

    modeler.initialize app
    app.router.post '/api/:modelName/:command', (next)->
      if @params.command is 'login' or @params.command is 'register'
        token = authentifier.createCookieToken(@body, secretToken)
        @response.userToken = token
        #res.header('uma', token)
        @cookies.set 'uma', token, expires: moment().add(30, 'days').toDate()
      yield next
