'use strict'
path = require 'path'
moment = require 'moment'
jwt = require 'jwt-simple'
firstRequest = undefined

createCookieToken = (user, secretToken)->
  expires = moment().add(30, 'days').valueOf()
  token = jwt.encode({
    iss: user.id
    exp: expires
  }, secretToken)
  return token

module.exports =
  initialize: (app, UserModel, AccountModel, secretToken, cookieName)->
    app.router.use '/*', (next) ->
      token = @body and @body[cookieName] or @query and @query[cookieName] or @headers[cookieName] or @cookies.get(cookieName)

      if token
        try
          decoded = jwt.decode(token, secretToken)
          if not decoded.exp <= Date.now()
            user = @request.user = yield UserModel.findOneD { '_id': decoded.iss }
            account = @request.account = yield AccountModel.findOneD { '_id': user.accountId }
            token = authentifier.createCookieToken(user, secretToken)
            @response.userToken = token
            #res.header('uma', token)
            @cookies.set 'uma', token, expires: moment().add(30, 'days').toDate()
        catch err

      if not firstRequest
        app.notify 'APLICATION FIRST REQUEST, CHECKING IF ONE OF THE ADMINS EXISTS'
        admins = yield UserModel.findD({role: 'ADMIN'})
        if admins.length is 0
          app.notify 'No admins found, creating one'
          defaultAdmin = new UserModel({role: 'ADMIN', email: 'q1@q1.ru', password: '123123123'})
          try
            yield defaultAdmin.saveD()
          catch e
            log e

          app.notify 'Created new admin'
        firstRequest = true
      yield next

  createCookieToken: createCookieToken.bind @
