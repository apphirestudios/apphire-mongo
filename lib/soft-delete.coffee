module.exports = (mongo)->
  mongo.attributes.deleted = {type: Boolean, required: true, default: false}
  mongo.commands.statics.removeD = (query) -> async =>
    result yield @findOneAndUpdate query, {'deleted': true}, null, (err, result) ->
    if not result? then throw new Error(@model.modelName + ' not found')
    return result

  mongo.transformers.push (query, action)->
    if action is 'findD' or action is 'findOneD'
      query.deleted = false

    if action is 'aggregateD'
      deletedFilter =[
        {
          $match:
            deleted: false
        }
      ]
      query = deletedFilter.concat query