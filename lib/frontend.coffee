cache = {}
ee = require './event-emitter'
async = require 'co'
gettersetter = (store) ->
  p = ->
    if arguments.length
      store = arguments[0]
    return store

  p.toJSON = ->
    return store
  return p

###
 * [createModelAttribute конструирует и возвращает расширенный геттер-сеттер с поддержкой вложенных атрибутов]
 * @param  {[Schema]} schema [Схема или ее часть, в формате Mongoose]
 * @param  {[Object]} store  [Инициализирующее значение модели - объект]
 * @return {[Function]}        [Геттер-сеттер с дополнительными фичами]
 *
 * Собсна фичи
 * 1) Устанавливает значения только тех атрибутов, которые есть в схеме, прочие игнорит
 * 2) Все вложенные атрибуты превращает в аналогичные расширенные геттеры-сеттеры
 * 3) Встроенная функция valid(), возвращает статус валидации
 * 4) Встроенная ф ункция touched(), геттерсеттер, true, если значение было изменено
 * 5) Переопределяет toJSON, сериализуя геттер-сеттеры в нормальные объекты
###

addChildrenEventsRetranslator = (prop, childProp)->
  childProp.on 'change', (params)->
    prop.__ee.trigger 'change', [params]


createModelAttribute = (schema, store, parent)->
  prop = (model, options)->
    if arguments.length
      if prop.schema instanceof Array
        #prop.store = createModelAttribute(prop.schema[0], undefined, prop)
        arr = []
        if model? then arr = model.map (arrayElement)->
          childProp = createModelAttribute(prop.schema[0], arrayElement, prop)
          addChildrenEventsRetranslator(prop, childProp)
          return childProp

        arr.push = (element)->
          childProp = createModelAttribute(prop.schema[0], element, prop)
          addChildrenEventsRetranslator(prop, childProp)
          Array.prototype.push.apply(@,[childProp])
        prop.store = arr
      else
        if not hasObjectsInAttributes prop.schema
          prop.__touched = true
          oldVal = prop.store
          newVal = if model? then model else prop.schema.default
          prop.store = newVal
          if not (options?.silent? and options.silent is true)
            prop.__ee.trigger 'change', [
              emitter: prop
              old: oldVal
              new: newVal
            ]

        else
          #prop.store = {}
          #for attribute, description of schema
          #  childProp = createModelAttribute(description, model?[attribute], prop)
          #  addChildrenEventsRetranslator(prop, childProp)
          # prop.store[attribute] = childProp

          # Это если захотим не создавать вложенные сторы каждый раз при сеттере
          if not prop.store? or options?.reset
            prop.store = {}
            for attribute, description of schema
              childProp = createModelAttribute(description, model?[attribute], prop)
              addChildrenEventsRetranslator(prop, childProp)
              prop.store[attribute] = childProp
          else
            for attribute, description of schema
              prop.store[attribute](model?[attribute], options)


    return prop.store

  prop.__ee = new ee()
  prop.on = -> prop.__ee.on.apply prop.__ee, arguments
  prop.valid = ->
    validate prop

  prop.touched = ->
    checkIfTouched(prop)

  prop.store = undefined
  prop.schema = schema
  prop.parent = parent

  prop.toJSON = ()->
    serialize prop

  prop store
  prop.__touched = false

  return prop


###
 * [Model загружает схему модели с сервера либо кэша, конструирует и возвращает класс модели]
 * @param  {[String]} name [Имя модели]
 * @return {[Function]}        [Класс модели, содержащий в т.ч. статические методы]
 ###
Model = (name)-> async ->
  if cache[name] then return cache[name]

  endpoint = (Model.apiPath or '/api/') + name + '/'

  schema = yield Model.http.post endpoint + "Schema"

  modelClass = cache[name] = (data)->
    modelInstance = createModelAttribute(schema, data)
    modelInstance.save = -> async ->
      response = yield Model.http.post endpoint + 'save',
        document: modelInstance.toJSON()
      return modelInstance response, {reset: true}


    modelInstance.remove = -> async ->
      return yield Model.http.post endpoint + 'remove',
        query:
          _id: modelInstance().id()

    for f, fn of schema.methods
      do ->
        fName = f
        fOptions = fn
        modelInstance[fName] = (args...)-> async ->
          response = yield Model.http.post endpoint + fName,
            arguments: args
            document: modelInstance.toJSON()
          #Если сервер запретил кастинг к типу модели, то возвращаем тупо сырой ответ
          if fOptions.disableTypeCasting then return response else return modelInstance(response)

    #Свойство vm - единственное место, куда можно складывать связанные атрибуты view-модели
    modelInstance.vm = gettersetter({})
    #Object.freeze modelInstance
    return modelInstance

  modelClass.findOne = (query, fields, options)-> async ->
    ret = yield Model.http.post endpoint + 'findOne',
      query: query
      fields: fields
      options: options
    return new modelClass ret

  modelClass.findById = (id)-> async ->
    ret = yield Model.http.post endpoint + 'findOne',
      query:
        _id: id
    return new modelClass ret

  modelClass.find = (query, fields, options)-> async ->
    ret = yield Model.http.post endpoint + 'find',
      query: query
      fields: fields
      options: options
    return ret.map (elem)-> new modelClass elem

  modelClass.aggregate = (query)-> async ->
    return yield Model.http.post endpoint + 'aggregate',
      query: query
  modelClass.count = (query)-> async ->
    return yield Model.http.post endpoint + 'count',
      query: query

  modelClass.remove = (query)-> async ->
    return yield Model.http.post endpoint + 'remove',
      query: query

  for f, fn of schema.statics
    do ->
      fName = f
      fOptions = fn
      modelClass[fName] = (args...)-> async ->
        ret = yield Model.http.post endpoint + fName,
          arguments: args
        if fOptions.disableTypeCasting then return ret else return new modelClass ret

  return modelClass

serialize = (prop)->
  return prop() if prop.schema?.type?
  res = {}
  for attribute, value of prop()
    if value() instanceof Array
      res[attribute] = value().map (element)->
        serialize element
    else
      res[attribute] = serialize value
  res


checkIfTouched = (prop)->
  if typeof prop.store == 'object' and not prop.schema.type?
    for attribute, value of prop.store
      if checkIfTouched(value) then return yes
    return no
  else
    return prop.__touched

validate = (prop)->
  prop.valid.error = gettersetter ''

  if typeof prop.store == 'object' and not prop.schema.type?
    if prop.store instanceof Array
      for sub in prop.store
        if not validate sub then return no
      return yes
    else
      for attribute, value of prop.store
        if not validate value then return no
      return yes


  if prop.schema.clientValidator
    if not Model.validators?[prop.schema.clientValidator]
      throw new Error 'Custom validator function' + prop.schema.clientValidator + ' is not registered in Model' + name
    if not Model.validators[prop.schema.clientValidator](prop.store)
      prop.valid.error prop.schema.validate[1]
      return no
    return yes


  if prop.schema.required
    if not prop.store?
      prop.valid.error 'Это поле обязательно нужно заполнить'
      return no

  if prop.schema.minlength
    if prop.store.length < prop.schema.minlength[0]
      prop.valid.error prop.schema.minlength[1] or ('Минимальное количество символов - ' +  prop.schema.minlength[0])
      return no


  return yes



hasObjectsInAttributes = (obj)->
  res = false
  for k, v of obj
    if (typeof v == 'object') and not (v instanceof Array) and k isnt 'default'
      #HACK - атрибут default часто бывает типа Дата, что считается объектом, поэтому отбросим

      res = true
  res

Model.createModelAttribute = createModelAttribute

module.exports = Model